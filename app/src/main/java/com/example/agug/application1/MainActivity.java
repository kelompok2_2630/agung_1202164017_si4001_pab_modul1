package com.example.agug.application1;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;


public class MainActivity extends AppCompatActivity {

    private EditText edtAlas;
    private EditText edtTinggi;
    private TextView edtHasil;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }

    public void hitung(View view) {
        edtAlas = findViewById(R.id.editText);
        edtTinggi = findViewById(R.id.editText2);
        edtHasil = findViewById(R.id.textView);

        Integer alas = Integer.parseInt(edtAlas.getText().toString());
        Integer tinggi = Integer.parseInt(edtTinggi.getText().toString());
        Integer textView = alas*tinggi;
        edtHasil.setText(String.valueOf(textView));
    }
}
